# 🌐 Zsh Commit Plugin: Simplifying Git Commit Workflows

<p align="center">
  <a href="https://gitlab.com/zshplugins/commit" target="blank"><img src="https://git-scm.com/images/logos/downloads/Git-Icon-1788C.png" width="200" alt="Git Logo" /></a>
</p>

<p align="center">A plugin for Zsh that simplifies Git commit workflows with predefined commit types and convenient "Oh Shit" options for quick fixes.</p>


---

## 👨‍💻 About the Developer: David Vanmak
Welcome! I'm David Vanmak, a developer with deep expertise in Zsh and Git. I focus on creating tools that enhance developer productivity and streamline workflows. This project reflects my commitment to these principles.

## 🚀 Project Highlights
- **Zsh Plugin Mastery 🐚**: Simplifying complex Git workflows with easy-to-use commands.
- **Predefined Commit Types 📝**: Speeding up the commit process with structured commit messages.
- **Quick Fixes 🛠️**: Providing "Oh Shit" options to quickly amend, rebase, or undo commits.
- **Enhanced Productivity ⚡**: Reducing the cognitive load of managing Git commits.

## 🛠️ Features
- **Commit Types 🌟**: Adopting standardized commit messages for clarity and consistency.
- **Oh Shit Options ⚙️**: Enabling rapid fixes for common Git mistakes.
- **Code Quality 📈**: Encouraging best practices and clean commit history.
- **Zsh Integration 🖥️**: Seamlessly integrating with Zsh for a smooth user experience.

## 📚 Quick Setup
1. **Clone the Repository 📥**: 
    ```bash
    git clone https://gitlab.com/zshplugins/commit.git ~/.oh-my-zsh/custom/plugins/commit
    ```
2. **Add the Plugin to Oh My Zsh 🛠️**: 
   - Open your `.zshrc` file in a text editor:
     ```bash
     nano ~/.zshrc
     ```
   - Add `commit` to the list of plugins:
     ```zsh
     plugins=(git commit)
     ```
3. **Reload Zsh Configuration 🔄**: 
    ```bash
    source ~/.zshrc
    ```

## 📐 Architectural Overview
- **Commit Types 🧠**: Using predefined commit types for consistency.
- **Oh Shit Options 💼**: Providing quick commands for amending, rebasing, and undoing commits.
- **Integration 🌐**: Ensuring seamless functionality with Zsh and Git.

## 🌟 Why Choose This Plugin
- **Simplified Git Workflows 💡**: Making Git easier to use with structured commands.
- **Focus on Quality 🏛️**: Maintaining a clean and understandable commit history.
- **Efficiency Boost 🎖️**: Reducing the time spent on common Git tasks.

## 🛡️ Testing & Security
- Comprehensive testing suite to ensure reliability and stability 🛡️.
- A security-first mindset to protect your development workflow 🔒.

## 📢 Connect with Me
Interested in discussing this project or exploring collaboration opportunities? Connect with me on [LinkedIn](https://www.linkedin.com/in/davidvanmak/). I’m always eager to engage in innovative projects and challenging development tasks!
