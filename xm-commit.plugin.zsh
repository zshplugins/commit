# Définir les types de commits prédéfinis avec les préfixes souhaités et les crochets
_commit_types=(
  "chore:[CHORE] 🧹"
  "update:[UPDATE] ⬆"
  "hotfix:[HOTFIX] 🚑"
  "add:[ADD] ➕"
  "fix:[FIX] 🐛"
  "refactor:[REFACTOR] 🔨"
  "feat:[FEAT] ✨"
  "docs:[DOCS] 📚"
  "style:[STYLE] 💄"
  "test:[TEST] 🧪"
)

# Ajouter le type de commit ohshit à la fin
_commit_types+=("ohshit:ohshit 🚨")

# List of ohshit options with Git commands
declare -A _ohshit_options
_ohshit_options=(
  ["magic-time-machine"]="git reflog"
  ["amend-commit"]="git add .; git commit --amend --no-edit"
  ["change-last-commit-message"]="git commit --amend"
  ["cancel-last-commit"]="git reset --soft HEAD~1"
)

# Fonction pour générer les messages de commit
xm_commit() {
  case "$1" in
    "ohshit")
      xm_ohshit "${@:2}"
      ;;
    *)
      local commit_prefix=""
      for ct in "${_commit_types[@]}"; do
        IFS=":" read -r short long <<< "$ct"
        if [ "$1" = "$short" ]; then
          commit_prefix="$long"
          break
        fi
      done

      if [ -z "$commit_prefix" ]; then
        echo "Type de commit inconnu: $1"
        return 1
      fi

      shift
      local message="$@"

      if [ -z "$message" ]; then
        echo "Usage: xm <type> <message>"
        return 1
      fi

      # Ajouter un hook de pré-commit pour vérifier le message
      if ! pre_commit_hook "$commit_prefix $message"; then
        echo "Pré-commit hook a échoué. Commit annulé."
        return 1
      fi

      git add .
      git commit -m "$commit_prefix $message"
      ;;
  esac
}

# Fonction pour gérer les options ohshit
xm_ohshit() {
  case "$1" in
    "magic-time-machine")
      git reflog
      ;;
    "amend-commit")
      git add . && git commit --amend --no-edit
      ;;
    "change-last-commit-message")
      git commit --amend
      ;;
    "cancel-last-commit")
      git reset --soft HEAD~1
      ;;
    *)
      echo "Unknown ohshit option: $1"
      return 1
      ;;
  esac
}

# Hook de pré-commit pour valider le message
pre_commit_hook() {
  local message="$1"

  # Exemple de validation : longueur minimale de message
  if [ ${#message} -lt 10 ]; then
    echo "Le message de commit est trop court. Doit être au moins de 10 caractères."
    return 1
  fi

  # Validation du format du commit
  if ! [[ $message =~ ^\[[A-Z]+\] ]]; then
    echo "Le message de commit doit commencer par un type entre crochets, ex: [FIX], [FEAT]."
    return 1
  fi

  return 0
}

# Auto-complétion pour la commande xm
_xm_completions() {
  local -a commit_types
  local -a ohshit_types

  commit_types=(
    'chore:[CHORE] 🧹'
    'update:[UPDATE] ⬆'
    'hotfix:[HOTFIX] 🚑'
    'add:[ADD] ➕'
    'fix:[FIX] 🐛'
    'refactor:[REFACTOR] 🔨'
    'feat:[FEAT] ✨'
    'docs:[DOCS] 📚'
    'style:[STYLE] 💄'
    'test:[TEST] 🧪'
    'ohshit:ohshit 🚨'
  )

  ohshit_types=(
    'magic-time-machine:Oh shit, I did something terribly wrong, please tell me git has a magic time machine!?!'
    'amend-commit:Oh shit, I committed and immediately realized I need to make one small change!'
    'change-last-commit-message:Oh shit, I need to change the message on my last commit!'
    'cancel-last-commit:Oh shit, I need to cancel my last commit!'
  )

  if ((CURRENT == 2)); then
    _describe 'commit type' commit_types
  elif ((CURRENT == 3)) && [[ "$words[2]" == "ohshit" ]]; then
    _describe 'ohshit options' ohshit_types
  fi
}

# Enregistrer la commande et l'auto-complétion
compdef _xm_completions xm_commit

# Alias pour la commande
alias xm='xm_commit'
